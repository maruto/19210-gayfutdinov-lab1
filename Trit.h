#ifndef INC_19210_GAYFUTDINOV_LAB1_TRIT_H
#define INC_19210_GAYFUTDINOV_LAB1_TRIT_H
#include<iostream>

enum trit{
    True = 1,
    Unknown = 0,
    False = 2
};

class Trit {
public:
    trit value;
    Trit();
    Trit(trit inValue);

    operator trit() const;

    Trit operator &(const Trit& rTrit) const;
    Trit operator &(const trit& rtrit) const;
    friend Trit operator &(const trit& ltrit, const Trit& rTrit) ;

    Trit operator |(const Trit& rTrit) const;
    Trit operator |(const trit& rtrit) const;
    friend Trit operator |(const trit& ltrit, const Trit& rTrit);

    bool operator ==(const Trit& rTrit) const;
    bool operator ==(const trit& rtrit) const;
    friend bool operator ==(const trit& ltrit, const Trit& rTrit);

    Trit operator !() const;

    friend std::ostream& operator<<(std::ostream& stream, Trit t);
};
#endif
