#include "Trit.h"

Trit::Trit(){
    value = Unknown;
}
Trit::Trit(trit inValue){
    value = inValue;
};

Trit::operator trit() const{
    return value;
};


Trit Trit::operator &(const Trit& rTrit) const{
    if(value == False || rTrit.value == False)
        return False;
    else if(value == True && rTrit.value == True)
        return True;
    else
        return Unknown;
};
Trit Trit::operator &(const trit& rtrit) const{
    if(value == False || rtrit == False)
        return False;
    else if(value == True && rtrit == True)
        return True;
    else
        return Unknown;
};
Trit operator &(const trit& ltrit, const Trit& rTrit) {
    if(ltrit == False || rTrit.value == False)
        return False;
    else if(ltrit == True && rTrit.value == True)
        return True;
    else
        return Unknown;

};


Trit Trit::operator |(const Trit& rTrit) const{
    if(value == True || rTrit.value == True)
        return True;
    else if(value == False && rTrit.value == False)
        return False;
    else
        return Unknown;
};
Trit Trit::operator |(const trit& rtrit) const{
    if(value == True || rtrit == True)
        return True;
    else if(value == False && rtrit == False)
        return False;
    else
        return Unknown;
};
Trit operator |(const trit& ltrit, const Trit& rTrit){
    if(ltrit == True || rTrit.value == True)
        return True;
    else if(ltrit == False && rTrit.value == False)
        return False;
    else
        return Unknown;
};


bool Trit::operator ==(const Trit& rTrit) const{
    if(value == rTrit.value)
        return true;
    else
        return false;
};
bool Trit::operator ==(const trit& rtrit) const{
    if(value == rtrit)
        return true;
    else
        return false;
};
bool operator ==(const trit& ltrit, const Trit& rTrit){
    if(ltrit == rTrit.value)
        return true;
    else
        return false;
};


Trit Trit::operator !() const{
    if(value == True)
        return False;
    else if(value == False)
        return True;
    else
        return Unknown;
};


std::ostream& operator<<(std::ostream& stream, Trit trit){
    if(trit.value == False)
        stream << "False";
    else if(trit.value == True)
        stream << "True";
    else
        stream << "Unknown";
    return stream;
};
